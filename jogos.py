import adivinhacao
import forca

def escolheJogo():
    print("+----------------------+")
    print("|    Escolha o jogo    |")
    print("+----------------------+")

    jogo = int(input("(1) -> Advinhação\n(2) -> forca\n\nDigite: "))

    if jogo == 1:
        adivinhacao.jogar()
    elif jogo == 2:
        forca.jogar()


if (__name__ == "__main__"):
    escolheJogo()