import random

caractere_vazio = "_"

def cabecalho():
    print("+---------------------+")
    print("|    Jogo de forca    |")
    print("+---------------------+")


def montar_palavra_secreta():
    arquivo = open("palavras.txt", "r")
    palavras = []

    for linha in arquivo:
        palavras.append(linha.strip().upper())

    arquivo.close()

    return palavras[random.randrange(0, len(palavras))].upper()


def montar_letras_acertadas(palavra):
    return [caractere_vazio for letra in palavra]


def pede_chute():
    return input("Letra: ").strip().upper()


def marca_chute_correto(palavra, letras, chute):
    index = 0
    for letra in palavra:
        if chute == letra.upper():
            letras[index] = letra

        index += 1

def imprime_mensagem_vencedor():
    print("Parabéns, você ganhou!")
    print("       ___________      ")
    print("      '._==_==_=_.'     ")
    print("      .-\\:      /-.    ")
    print("     | (|:.     |) |    ")
    print("      '-|:.     |-'     ")
    print("        \\::.    /      ")
    print("         '::. .'        ")
    print("           ) (          ")
    print("         _.' '._        ")
    print("        '-------'       ")

def mensagem_erro(erros, letras_acertadas):
    print('Ainda faltam acertar {} letras e {} tentativas'.format(str(letras_acertadas.count(caractere_vazio)), erros))


def se_fudeu(palavra_secreta):
    print("Se fudeu!")
    print("A palavra era {}".format(palavra_secreta))
    print("    _______________         ")
    print("   /               \       ")
    print("  /                 \      ")
    print("//                   \/\  ")
    print("\|   XXXX     XXXX   | /   ")
    print(" |   XXXX     XXXX   |/     ")
    print(" |   XXX       XXX   |      ")
    print(" |                   |      ")
    print(" \__      XXX      __/     ")
    print("   |\     XXX     /|       ")
    print("   | |           | |        ")
    print("   | I I I I I I I |        ")
    print("   |  I I I I I I  |        ")
    print("   \_             _/       ")
    print("     \_         _/         ")
    print("       \_______/           ")

def desenha_forca(erros):
    print("  _______     ")
    print(" |/      |    ")

    if(erros == 7):
        print(" |      (_)   ")
        print(" |            ")
        print(" |            ")
        print(" |            ")

    if(erros == 6):
        print(" |      (_)   ")
        print(" |      \     ")
        print(" |            ")
        print(" |            ")

    if(erros == 5):
        print(" |      (_)   ")
        print(" |      \|    ")
        print(" |            ")
        print(" |            ")

    if(erros == 4):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |            ")
        print(" |            ")

    if(erros == 3):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |       |    ")
        print(" |            ")

    if(erros == 2):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |       |    ")
        print(" |      /     ")

    if (erros == 1):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |       |    ")
        print(" |      / \   ")

    print(" |            ")
    print("_|___         ")
    print()

def jogar():
    cabecalho()

    palavra_secreta = montar_palavra_secreta()

    letras_acertadas = montar_letras_acertadas(palavra_secreta)
    print(letras_acertadas)

    acertou = False
    enforcou = False
    erros = 7

    while not enforcou and not acertou:

        chute = pede_chute()

        if chute in palavra_secreta:
            marca_chute_correto(palavra_secreta, letras_acertadas, chute)
        else:
            desenha_forca(erros)
            erros -= 1

        print(letras_acertadas, "\n")

        acertou = caractere_vazio not in letras_acertadas

        if acertou:
            imprime_mensagem_vencedor()
        else:
            mensagem_erro(erros, letras_acertadas)

        if erros == 0:
            se_fudeu(palavra_secreta)
            enforcou = True

    print("\n********\n")

if __name__ == "__main__":
    jogar()