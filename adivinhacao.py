import random

def jogar():
    print("+---------------------+")
    print("| Jogo de adivinhação |")
    print("+---------------------+")

    numero_secreto = random.randrange(1, 101)
    total_de_tentativas = 0
    pontos = 1000

    # print(numero_secreto)

    dif = int(input("Qual o nivel de dificuldade:\n(1) -> facil\n(2) -> medio\n(3) -> dificil\n\nDigite:"))

    if dif == 1:
        total_de_tentativas = 20
    elif dif == 2:
        total_de_tentativas = 10
    else:
        total_de_tentativas = 5

    for rodada in range(1, total_de_tentativas + 1):

        print("\nRodada {} de {}: ".format(rodada, total_de_tentativas), end=" ")
        chute_str = input("Digite um número entre 1 e 100: ")
        print("Voce digitou:", chute_str)
        chute = int(chute_str)
        if (chute < 1 or chute > 100):
            print("Óia a cagada\n")
            continue

        acertou = chute == numero_secreto
        maior = chute > numero_secreto
        menor = chute < numero_secreto

        if (acertou):
            print("ACERTOU")
            break
        else:
            if (maior):
                print("EROU pra cima")
            elif (menor):
                print("EROU pra baixo")
            else:
                print("uai")

            pontos_perdidos = abs(numero_secreto - chute)
            pontos = pontos - pontos_perdidos

    print("O número era: ", numero_secreto, "\nPontos:", pontos)
    print("\n********\n")

if (__name__ == "__main__"):
    jogar()